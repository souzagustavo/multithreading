package main

import (
	"gitlab.com/souzagustavo/multithreading/config"
	"gitlab.com/souzagustavo/multithreading/internal/use_case"
)

func main() {
	cfg := config.GetConfig()

	viaCepApi := use_case.NewViaCepApi(cfg.GetViaCepApi(), cfg.GetTimeout())
	cdnCepApi := use_case.NewCdnCepApi(cfg.GetCdnCepApi(), cfg.GetTimeout())

	cepApis := []use_case.CepApiI{viaCepApi, cdnCepApi}

	m := use_case.NewMultithreading(cepApis, cfg.GetTimeout())

	m.GetFastestCepResponse(cfg.Cep)
}
