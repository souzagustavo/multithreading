package dto

import (
	"strconv"
	"strings"
)

type ResponseChannel struct {
	URL    string //Endereço do URL da requisição
	Body   string //Corpo da resposta HTTP
	Status int    //Status da resposta HTTP
}

func (ch ResponseChannel) String() string {
	sb := strings.Builder{}

	sb.WriteString("URL: " + ch.URL + "\n")
	sb.WriteString("Response: \n")
	sb.WriteString("- Status: " + strconv.Itoa(ch.Status) + "\n")
	sb.WriteString("- Body: " + ch.Body + "\n")
	return sb.String()
}
