package use_case

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCepApi_formatCep(t *testing.T) {

	cepApi := CepApi{}

	t.Run("Only digits", func(t *testing.T) {
		cepFormat, err := cepApi.formatCep("89223262")
		assert.NoError(t, err)
		assert.Equal(t, "89223-262", cepFormat)
	})

	t.Run("Digits with separator - ", func(t *testing.T) {
		cepFormat, err := cepApi.formatCep("89223-262")
		assert.NoError(t, err)
		assert.Equal(t, "89223-262", cepFormat)
	})

	t.Run("Invalid length", func(t *testing.T) {
		_, err := cepApi.formatCep("89223-2622")
		assert.Error(t, err)
		assert.Equal(t, ErrCepPattern, err)
	})

	t.Run("Invalid separator", func(t *testing.T) {
		_, err := cepApi.formatCep("89223*262")
		assert.Error(t, err)
		assert.Equal(t, ErrCepPattern, err)
	})

	t.Run("Invalid numbers", func(t *testing.T) {
		_, err := cepApi.formatCep("89xx3-262")
		assert.Error(t, err)
		assert.Equal(t, ErrCepPattern, err)

		_, err = cepApi.formatCep("89xx3262")
		assert.Error(t, err)
		assert.Equal(t, ErrCepPattern, err)
	})
}
