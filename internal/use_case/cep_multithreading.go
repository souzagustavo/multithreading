package use_case

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/souzagustavo/multithreading/internal/model/dto"
)

type Multithreading struct {
	cepApis []CepApiI
	timeout time.Duration
}

func NewMultithreading(cepApis []CepApiI, timeout int) *Multithreading {
	return &Multithreading{
		cepApis: cepApis,
		timeout: time.Duration(timeout),
	}
}

func (m *Multithreading) GetFastestCepResponse(cep string) {
	//cria contexto com timeout passado como parâmetro
	ctx, cancel := context.WithTimeout(context.Background(), m.timeout*time.Millisecond)
	defer cancel()

	//cria canal para receber confirmação de resposta
	respChannel := make(chan dto.ResponseChannel)
	//ao final da função, canal será fechado
	defer func() {
		close(respChannel)
		println("Cep Response channel closed")
	}()

	//para cada url executa go routine solicitando CEP
	for _, api := range m.cepApis {
		go api.GetCep(ctx, cep, respChannel)
	}

	select {
	//aguarda resposta de alguma API
	case ack := <-respChannel:
		m.handleAck(&ack)
	//em caso de timeout
	case <-ctx.Done():
		m.handleTimeout()
	}

}

// exibe no console informação de timeout
func (m *Multithreading) handleTimeout() {
	fmt.Println("API Calls timeout")
}

// exibe no console informações da confirmação de resposta
func (m *Multithreading) handleAck(ack *dto.ResponseChannel) {
	fmt.Println(ack.String())
}
