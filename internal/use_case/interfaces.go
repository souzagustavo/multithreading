package use_case

import (
	"context"

	"gitlab.com/souzagustavo/multithreading/internal/model/dto"
)

type CepApiI interface {
	//parâmetros:
	// - cep: codigo de endereçamento postal (apenas dígitos)
	// - resp: canal para informação confirmação de resposta
	GetCep(ctx context.Context, cep string, resp chan<- dto.ResponseChannel)
}
