package use_case

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/souzagustavo/multithreading/internal/model/dto"
)

var (
	ErrCepPattern = fmt.Errorf("Cep should follow the pattern 99999-888 or 99999888")
)

type CepApi struct {
	//endereço da API com o CEP formatado como %s. Por exemplo: http://dns.com.br?cep=%s ou http://dns/%s.json
	url string
	//timeout: tempo limite para executar requisição em milisegundos
	timeout time.Duration
}

// construtor para VIA CEP API
func NewViaCepApi(viaCepUrl string, timeout int) *CepApi {
	return newCepApi(viaCepUrl, timeout)
}

// construtor para CDN CEP API
func NewCdnCepApi(cdnCepUrl string, timeout int) *CepApi {
	return newCepApi(cdnCepUrl, timeout)
}

// construtor privado
func newCepApi(url string, timeout int) *CepApi {
	return &CepApi{
		url:     url,
		timeout: time.Duration(timeout),
	}
}

func (api *CepApi) GetCep(ctx context.Context, cep string, resp chan<- dto.ResponseChannel) {
	cep, err := api.formatCep(cep)
	if err != nil {
		log.Println(err.Error())
		return
	}
	//cria contexto com timeout da API
	ctx, cancel := context.WithTimeout(ctx, api.timeout*time.Millisecond)
	defer cancel()

	//cria URL da requisição substituindo %s pelo valor do CEP
	urlWithCep := fmt.Sprintf(api.url, cep)
	//cria DTO de confirmação de resposta
	ack := dto.ResponseChannel{URL: urlWithCep}

	log.Println("Creating GET req: " + ack.URL)
	//cria requisição com contexto de timeout
	req, err := http.NewRequestWithContext(ctx, "GET", urlWithCep, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Executing GET req to: " + ack.URL)
	//executa requisição
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err.Error())
		return
	}
	//atribui status ao ACK
	if response.StatusCode != 200 {
		log.Println("Status != 200")
		return
	}
	ack.Status = response.StatusCode
	//body sempre deve ser fechado
	defer response.Body.Close()
	//leitura dos bytes da resposta http
	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println(err.Error())
		return
	}
	//atribui body ao ACK
	ack.Body = string(body)

	log.Println("Sending ack to channel:" + ack.URL)
	//publica no canal ACK (resposta) finalizada
	resp <- ack
}

func (api *CepApi) formatCep(cep string) (string, error) {
	//tamanho do cep
	length := len(cep)

	//valida se cep tem tamanho 8 ou 9
	if length != 8 && length != 9 {
		return cep, ErrCepPattern
	}

	if length == 8 {
		//se tamanho for 8, deve possuir apenas digitos
		if _, err := strconv.Atoi(cep); err != nil {
			return cep, ErrCepPattern
		}
		//retornar cep com hífen
		return fmt.Sprintf("%s-%s", cep[:5], cep[5:]), nil
	} else {
		//se tamanho for 9, deve possuir um hífen
		cepSplit := strings.Split(cep, "-")

		//verifica se cep possui digitos a esquerda e a direita do hífen
		if len(cepSplit) != 2 {
			return cep, ErrCepPattern
		}
		for _, cepPart := range cepSplit {
			if _, err := strconv.Atoi(cepPart); err != nil {
				return cep, ErrCepPattern
			}
		}
		return cep, nil
	}
}
