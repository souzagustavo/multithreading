package config

import (
	"flag"
	"fmt"

	"github.com/spf13/viper"
)

type config struct {
	ApiViaCep string `mapstructure:"API_VIA_CEP"`
	ApiCdnCep string `mapstructure:"API_CDN_CEP"`
	Timeout   int    `mapstructure:"TIMEOUT"`
	Cep       string
}

var cfg config

func init() {
	println("Initializing cfg: multithreading_config")

	//possibilita configurar path do arquivo de configurações .env através da flag -config
	path := flag.String("config", ".", "caminho arquivo de configuração")

	//parametro cep
	cep := flag.String("cep", "", "código postal")

	flag.Parse()

	//caminho onde estará o arquivo .env
	viper.AddConfigPath(*path)
	//nome da configuração
	viper.SetConfigName("multithreading_config")
	//tipo da configuração
	viper.SetConfigType("env")
	//arquivo de configurações
	viper.SetConfigFile(".env")
	//busca variáveis OS, caso não existam atribui os valores do arquivo de configurações
	viper.AutomaticEnv()

	//carrega variáveis para viper
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	//carrega config
	err = viper.Unmarshal(&cfg)
	if err != nil {
		panic(fmt.Errorf("fatal error unmarshal ctg: %w", err))
	}

	if *cep == "" {
		panic("Missing --cep or -cep flag")
	}
	cfg.Cep = *cep
}

func GetConfig() config {
	return cfg
}

func (c config) GetViaCepApi() string {
	return c.ApiViaCep
}

func (c config) GetCdnCepApi() string {
	return c.ApiCdnCep
}

func (c config) GetTimeout() int {
	return c.Timeout
}

func (c config) GetCep() string {
	return c.Cep
}
